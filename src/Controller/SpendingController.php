<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Campaign;
use App\Entity\Spending;
use App\Entity\Participant;


/**
 * @Route("/campaign")
 */
class SpendingController extends AbstractController
{
    /**
     * @Route("/{id}/spending", name="campaign_spending", methods="GET")
     */
    public function index(Campaign $campaign)
    {
        // return $this->render('spending/index.html.twig', [
        //     'controller_name' => 'SpendingController',
        // ]);
        return $this->render('campaign/spending.html.twig', compact("campaign"));
    }

    /**
     * @Route("/{id}/spending", name="spending_new", methods="POST")
     */
    public function new(Request $request, Participant $participant, Campaign $campaign): Respons
    {
        //enregistrer participant 
        $participant = new Participant();
        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));
        $participant->setCampaignId($request->request->get('campaign_id'));
        

        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();

        $spending = new Spending();
        $spending->setAmount( (int)$request->request->get('amount_spending') * 100 );
        $spending->setLabel($request->request->get('label'));
        //dd($participant->getId());
        $spending->setParticipant($participant);

        $em = $this->getDoctrine()->getManager();
        $em->persist($spending);
        $em->flush();
        
        return $this->render('campaign/spending.html.twig', compact("campaign"));
    }
}
