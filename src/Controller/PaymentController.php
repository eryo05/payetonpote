<?php

namespace App\Controller;

use App\Entity\Payment;
use App\Form\PaymentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Participant;
use App\Entity\Campaign;

/**
 * @Route("/payment")
 */
class PaymentController extends AbstractController
{
    /**
     * @Route("/charge", name="payment_charge", methods="POST")
     */
    public function charge(Request $request): Response
    {
        //enregistrer participant 
        $participant = new Participant();
        $participant->setName($request->request->get('name'));
        $participant->setEmail($request->request->get('email'));
        $participant->setCampaignId($request->request->get('campaign_id'));

        $em = $this->getDoctrine()->getManager();
        $em->persist($participant);
        $em->flush();

        $payment = new Payment();
        $payment->setAmount( (int)$request->request->get('mount') * 100 );
        $payment->setParticipantId($participant->getId());

        $em = $this->getDoctrine()->getManager();
        $em->persist($payment);
        $em->flush();

        try{
            // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey("sk_test_NUm0BaNLf5UXSqY6JNugB3Vh");

        // Token is created using Checkout or Elements!
        // Get the payment token ID submitted by the form:
        $token = $_POST['stripeToken'];

        $charge = \Stripe\Charge::create([
            'amount' => $request->request->get('mount') * 100,
            'currency' => 'eur',
            'description' => 'Example charge',
            'source' => $token,
        ]);
        }catch(\Exception $e){
            $this->addFlash(
                'error',
                'Le paiement a echoué. Raison : ' . $e->getMessage()
            );
        }
        

        return $this->redirectToRoute("campaign_show",['id' => $request->request->get('campaign_id')]);
        
    }

   

    
}
