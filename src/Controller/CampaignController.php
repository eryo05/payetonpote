<?php

namespace App\Controller;

use App\Entity\Campaign;
use App\Form\CampaignType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Participant;
use App\Entity\Payment;

/**
 * @Route("/campaign")
 */
class CampaignController extends AbstractController
{
    /**
     * @Route("/", name="campaign_index", methods="GET")
     */
    public function index(): Response
    {
        $campaigns = $this->getDoctrine()
            ->getRepository(Campaign::class)
            ->findAll();

        return $this->render('campaign/index.html.twig', ['campaigns' => $campaigns]);
    }

    /**
     * @Route("/new", name="campaign_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $campaign = new Campaign();
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);
        $campaign->setName($request->request->get('name'));

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $campaign->setId();
            $em->persist($campaign);
            $em->flush();

            return $this->redirectToRoute('campaign_show', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/new.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_show", methods="GET")
     */
    public function show(Campaign $campaign): Response
    {
         $em = $this->getDoctrine()->getManager();

        $RAW_QUERY = 'SELECT * FROM participant
        LEFT JOIN payment ON payment.participant_id = participant.id
        WHERE campaign_id ="'. $campaign->getId().'"';
        
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $results = $statement->fetchAll();
        //dd($results[1]["amount"]);


        $total_amount = 0;
        $total_participant = count($results);
        
        for($i = 0 ; $i <= ($total_participant - 1) ; $i++ )
        {
            $total_amount += $results[$i]["amount"];
        }
        $total_amount2 = $total_amount / 100;
        $objectif =  round($total_amount2 / $campaign->getGoal() * 100 );

        return $this->render('campaign/show.html.twig', compact("campaign", "results" , "total_participant" , "total_amount2" , "objectif"));
    }

    /**
     * @Route("/{id}/edit", name="campaign_edit", methods="GET|POST")
     */
    public function edit(Request $request, Campaign $campaign): Response
    {
        $form = $this->createForm(CampaignType::class, $campaign);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('campaign_edit', ['id' => $campaign->getId()]);
        }

        return $this->render('campaign/edit.html.twig', [
            'campaign' => $campaign,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="campaign_delete", methods="DELETE")
     */
    public function delete(Request $request, Campaign $campaign): Response
    {
        if ($this->isCsrfTokenValid('delete'.$campaign->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campaign);
            $em->flush();
        }

        return $this->redirectToRoute('campaign_index');
    }
     /**
     * @Route("/{id}/pay", name="campaign_pay", methods="GET|POST")
     */
    public function pay(Campaign $campaign): Response
    {
        $amount_participant = $_GET['amount_participant'] ?? "";
        return $this->render('campaign/pay.html.twig', compact('campaign','amount_participant'));
    }
}
